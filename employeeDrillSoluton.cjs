let fs = require('fs');
let path = require("path");

function dataBasedOnId(filename,callback) {
    let pathOfDatajson = path.join(__dirname, filename)
    fs.readFile(pathOfDatajson, "utf8", (err, data) => {
        if (err) {
            callback(err,null);
            return;
        } else {
            let employeeData=JSON.parse(data)
            let ids=[2, 13, 23];
            employeeData=employeeData.employees;
            let dataReceived=employeeData.filter((objectComing)=>ids.includes(objectComing.id))
            let jsonData=JSON.stringify(dataReceived);
        
            let employeeDataJsonFilepath=path.join(__dirname,"dataMatchingWithId.json")

            fs.writeFile(employeeDataJsonFilepath,jsonData,(err,data)=>{
                if(err){
                    callback(err,null);
                    return;
                }else{
                    callback(null,employeeDataJsonFilepath);
                }
            })
        }
    })
}

function groupingBasedOnCompanies(filename,callback) {
    let pathOfDatajson = path.join(__dirname, filename)
    fs.readFile(pathOfDatajson, "utf8", (err, data) => {
        if (err) {
            callback(err,null);
            return;
        } else {
            let givenData=JSON.parse(data);
            givenData=givenData.employees
            
            let groupObject={ "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []};

            let obtainedGroup=givenData.reduce((initialGroup,objectComing)=>{
                let companyName=objectComing.company
                if(initialGroup[companyName]){
                    initialGroup[companyName].push(objectComing)
                }
                return initialGroup;
            },groupObject)
        
            let groupByCompanyPath=path.join(__dirname,"groupingCompanies.json");
            let jsonData=JSON.stringify(obtainedGroup);

            fs.writeFile(groupByCompanyPath,jsonData,(err)=>{
                if(err){
                    callback(err,null);
                    return;
                }else{
                    callback(null,groupByCompanyPath);
                }
            })
        }
    })
}

function dataOfParticularCompany(filename,callback) {
    let pathOfDatajson = path.join(__dirname, filename)
    fs.readFile(pathOfDatajson, "utf8", (err, data) => {
        if (err) {
            callback(err,null);
            return;
        } else {
            let employeeData=JSON.parse(data)
            employeeData=employeeData.employees;
            let dataReceived=employeeData.filter((objectComing)=>objectComing.company==="Powerpuff Brigade")
        
            let companyPath=path.join(__dirname,"dataOfParticularCompany.json");
            let jsonData=JSON.stringify(dataReceived);

            fs.writeFile(companyPath,jsonData,(err)=>{
                if(err){
                    callback(err,null);
                    return;
                }else{
                    callback(null,companyPath);
                }
            })
        }
    })
}

function removingDataOfParticularCompany(filename,callback) {
    let pathOfDatajson = path.join(__dirname, filename)
    fs.readFile(pathOfDatajson, "utf8", (err, data) => {
        if (err) {
            callback(err,null);
            return;
        } else {
            let employeeData=JSON.parse(data)
            employeeData=employeeData.employees;
            let dataReceived=employeeData.filter((objectComing)=>objectComing.id!==2)
        
            let removingParticularCompany=path.join(__dirname,"removeDataMatched.json");
            let jsonData=JSON.stringify(dataReceived);

            fs.writeFile(removingParticularCompany,jsonData,(err)=>{
                if(err){
                    callback(err,null);
                    return;
                }else{
                    callback(null,removingParticularCompany);
                }
            })
        }
    })
}

function sortingData(filename,callback) {
    let pathOfDatajson = path.join(__dirname, filename)
    fs.readFile(pathOfDatajson, "utf8", (err, data) => {
        if (err) {
            callback(err,null);
            return;
        } else {
            let employeeData=JSON.parse(data)
            employeeData=employeeData.employees;
            let dataReceived=employeeData.sort((item1,item2)=>{
                if(item1.company<item2.company){
                    return -1;
                }
                else if(item1.company>item2.company){
                    return 1;
                }
                else{
                    if(item1.id<item2.id){
                        return -1;
                    }
                    else if(item1.id>item2.id){
                        return 1;
                    }
                    else{
                        return 0;
                    }
                }
            })
        
            let sortingDataByCompanyPath=path.join(__dirname,"sortingDataByCompany.json");
            let jsonData=JSON.stringify(dataReceived);

            fs.writeFile(sortingDataByCompanyPath,JSON.stringify(jsonData),(err)=>{
                if(err){
                    callback(err,null);
                    return;
                }else{
                    callback(null,sortingDataByCompanyPath);
                }
            })
        }
    })
}

function swapingBasedOnPsition(filename,callback) {
    let pathOfDatajson = path.join(__dirname, filename)
    fs.readFile(pathOfDatajson, "utf8", (err, data) => {
        if (err) {
            callback(err,null);
            return;
        } else {
            let employeeData=JSON.parse(data)
            employeeData=employeeData.employees;
            let result1 = employeeData.filter((item) => {
                return item.id === 92;
            })
            let result2 =employeeData.filter((item) => {
                return item.id === 93;
            })
            let newResult = employeeData.map((item) => {
                if (item.id === 92) {
                    return result2;
                }
                else if (item.id === 93) {
                    return result1;
                }
                return item;
            })
            
            let swappedDataPath=path.join(__dirname,"swappedData.json")
            let jsonData=JSON.stringify(newResult);

            fs.writeFile(swappedDataPath,jsonData,(err)=>{
                if(err){
                    callback(err,null);
                    return;
                }else{
                    callback(null,swappedDataPath);
                }
            })
        }
    })
}


function addingBirthDate(filename,callback) {
    let pathOfDatajson = path.join(__dirname, filename)
    fs.readFile(pathOfDatajson, "utf8", (err, data) => {
        if (err) {
            callback(err,null);
            return;
        } else {
            let employeeData=JSON.parse(data)
            employeeData=employeeData.employees;
            let dataReceived=employeeData.filter((objectComing)=>{
                if(objectComing.id%2===0){
                    return objectComing["date_of_birth"]=new Date();
                }
                
            })
        
            let addingDateOfBirth=path.join(__dirname,"addingDateOfBirth.json");
            let jsonData=JSON.stringify(dataReceived);

            fs.writeFile(addingDateOfBirth,jsonData,(err)=>{
                if(err){
                    callback(err,null);
                    return;
                }else{
                    callback(null,addingDateOfBirth);
                }
            })
        }
    })
}



module.exports={dataBasedOnId, groupingBasedOnCompanies,dataOfParticularCompany,removingDataOfParticularCompany,sortingData,swapingBasedOnPsition,addingBirthDate};